<?php
declare(strict_types=1);

use DI\ContainerBuilder;
use Monolog\Logger;

return function (ContainerBuilder $containerBuilder) {
    $rootPath = realpath(__DIR__ . '/..');

    $isDebug = (getenv('APPLICATION_ENV') != 'production');

    $containerBuilder->addDefinitions([
        'settings' => [
            'debug' => $isDebug,
            'temporary_path' => $rootPath . '/var/tmp',
            'route_cache' => $rootPath . '/var/cache/routes',

            // LDAP
            'ldap' => [
                'hostname' => getenv('LDAP_HOSTNAME'),
                'rdn' => getenv('LDAP_RDN'),
                'user' => getenv('LDAP_USER'),
                'password' => getenv('LDAP_PASSWORD')
            ],

            // View
            'view' => [
                'template_path' => $rootPath . '/templates',
                'twig' => [
                    'cache' => $rootPath . '/var/cache/twig',
                    'debug' => (getenv('APPLICATION_ENV') != 'production'),
                    'auto_reload' => true
                ]
            ],

            // Doctrine settings
            'doctrine' => [
                'meta' => [
                    'entity_path' => [ $rootPath . '/src/Entity' ],
                    'auto_generate_proxies' => true,
                    'proxy_dir' => $rootPath . '/var/cache/proxies',
                    'cache' => null
                ],
                'connection' => [
                    'driver' => 'pdo_mysql',
                    'host' => getenv('VIKINGRETRO_DB_HOST'),
                    'port' => 3306,
                    'dbname' => getenv('VIKINGRETRO_DB_DATABASE'),
                    'user' => getenv('VIKINGRETRO_DB_USERNAME'),
                    'password' => getenv('VIKINGRETRO_DB_PASSWORD'),
                    'charset' => 'utf8mb4'
                ]
            ],

            // Monolog settings
            'logger' => [
                'name' => 'VIKINGRETRO',
                // 'path' => $rootPath . '/var/log/app.log',
                'path' =>  getenv('docker') ? 'php://stdout' : $rootPath . '/var/log/app.log',
                'level' => $isDebug ? Logger::DEBUG : Logger::INFO
            ]
        ]
    ]);

    if (getenv('APPLICATION_ENV') == 'production') { // Should be set to true in production
        $containerBuilder->enableCompilation($rootPath . '/var/cache');
    }
};