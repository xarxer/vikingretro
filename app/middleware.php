<?php
declare(strict_types=1);

use Slim\App;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;

return function (App $app) {
    $app->add(TwigMiddleware::createFromContainer($app, Twig::class));
    $app->add(new \Slim\Middleware\Session([
        'name' => 'dummy_session',
        'autorefresh' => true,
        'lifetime' => '1 hour'
    ]));
};