<?php
declare(strict_types=1);

use DI\ContainerBuilder;
use Psr\Container\ContainerInterface;
use VikingRetro\Middleware\ApiAuthMiddleware;

return function (ContainerBuilder $containerBuilder) {
    // Renderer factories
    $containerBuilder->addDefinitions([
        VikingRetro\Renderer\HtmlErrorRenderer::class => function (ContainerInterface $c) {
            return new VikingRetro\Renderer\HtmlErrorRenderer($c);
        }
    ]);
};