<?php
declare(strict_types=1);

use DI\ContainerBuilder;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManager;
use Slim\Views\Twig;
use VikingRetro\System\LDAPSettings;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        'session' => function() {
            return new \SlimSession\Helper();
        },

        LoggerInterface::class => function (ContainerInterface $container) {
            $loggerSettings = $container->get('settings')['logger'];
            $logger = new Logger($loggerSettings['name']);

            $logger->pushProcessor(new UidProcessor());
            $logger->pushHandler(new StreamHandler($loggerSettings['path'], $loggerSettings['level']));

            return $logger;
        },

        EntityManager::class => function (ContainerInterface $container) {
            $doctrineSettings = $container->get('settings')['doctrine'];
            $config = Doctrine\ORM\Tools\Setup::createAnnotationMetadataConfiguration(
                $doctrineSettings['meta']['entity_path'],
                $doctrineSettings['meta']['auto_generate_proxies'],
                $doctrineSettings['meta']['proxy_dir'],
                $doctrineSettings['meta']['cache'],
                false
            );

            return EntityManager::create($doctrineSettings['connection'], $config);
        },

        Twig::class => function(ContainerInterface $container) {
            $settings = $container->get('settings');
            return new Twig(
                $settings['view']['template_path'],
                $settings['view']['twig']
            );
        },

        LDAPSettings::class => function(ContainerInterface $container) {
            $settings = $container->get('settings')['ldap'];
            return new LDAPSettings(
                $settings['hostname'],
                $settings['rdn'],
                $settings['user'],
                $settings['password']
            );
        }
    ]);
};