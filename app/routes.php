<?php
declare(strict_types=1);

use Slim\App;
use Slim\Routing\RouteCollectorProxy;
use VikingRetro\ApiController\AuthApiController;
use VikingRetro\ApiController\NotesApiController;
use VikingRetro\ApiController\RetrospectiveApiController;
use VikingRetro\Controller\ExampleController;
use VikingRetro\Controller\HomeController;
use VikingRetro\Controller\RetrospectiveController;
use VikingRetro\Middleware\ApiAuthMiddleware;
use VikingRetro\Middleware\AuthMiddleware;

return function (App $app) {

    /*
     * API endpoints
     */
    $app->group('/api', function(RouteCollectorProxy $group) {
        $group->post('/authenticate', AuthApiController::class . ':authenticate')->setName('api.authenticate');

        $group->get('/retrospective', RetrospectiveApiController::class . ':list')->setName('api.retrospective.list');
        $group->get('/retrospective/create', RetrospectiveApiController::class . ':create')->setName('api.retrospective.create');

        $group->get('/notes', NotesApiController::class . ':list')->setName('api.notes.list');
        $group->post('/notes/create', NotesApiController::class . ':create')->setName('api.notes.create');
    })->add(ApiAuthMiddleware::class);

    /*
     * Frontend endpoints
     */
    $app->group('', function(RouteCollectorProxy $group) {
        $group->get('/', HomeController::class . ':show')->setName('home.show');
        $group->get('/retrospective', RetrospectiveController::class . ':show')->setName('retrospective.show');
        $group->get('/example', ExampleController::class . ':show')->setName('example.show');
    })->add(AuthMiddleware::class);

};