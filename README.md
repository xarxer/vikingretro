# VectroSpective

A sprint retrospective is a great way for your team to reflect on the previous sprint, the work that was done, 
the goals achieved and generate new ideas for improvement or develop of new tools.

VectroSpective is the new modern approach of retrospective meetings for TeamVikings, an agile development team. 
VectroSpective started as a side project for two developers of team vikings with intention to automate the current "sticky notes on whiteboard"-meetings. 

In the long run we hope that other teams within the company to adopt to the new system.


### Installation

Docker is required by VectroSpective since its built inside a self-contained docker image. 

1. Clone the repo
    ```sh
    $ git clone https://gitlab.com/xarxer/vikingretro.git
    $ cd vikingretro
    ```

2. Load composer repositories with package information, update the dependencies and generate autoload files.
    ```sh
    $ composer install
    $ composer dumpautoload
    ```

3. Create a folder for logging and cache
    ```sh
    $ mkdir -p var
    $ sudo chown -hR www-data var           # Different OS has different name for "www-data" (Fix this)
    ```

4. Build the docker. 
    ```sh
    $ cd docker
    $ docker-compose build
    $ docker-compose up
    ```
   If you get `you're using the wrong Compose file version` then update `vikingretro/docker/docker-compose.yml` to the version installed in your computer.
   
5. Now its fully configured and reachable by the following links. The ports are configured in `vikingretro/docker/docker-compose.yml`

    | Service         | Location              |
    |-----------------|-----------------------|
    | Web application | http://localhost:8080 |
    | phpMyAdmin      | http://localhost:8000 |
    | phpLDAPadmin    | http://localhost:8081 |