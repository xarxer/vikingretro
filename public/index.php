<?php
declare(strict_types=1);

use DI\ContainerBuilder;
use Slim\Factory\AppFactory;
use Slim\Factory\ServerRequestCreatorFactory;
use Slim\Interfaces\RouteParserInterface;

// Absolute path to the root directory
$rootPath = realpath(__DIR__ . '/..');

// Include composer autoloader
require $rootPath . '/vendor/autoload.php';

// PHP-DI Container builder
$containerBuilder = new ContainerBuilder();

// Settings
$settings = require $rootPath . '/app/settings.php';
$settings($containerBuilder);

// Dependencies
$dependencies = require $rootPath . '/app/dependencies.php';
$dependencies($containerBuilder);

// Factories
$factories = require $rootPath . '/app/factories.php';
$factories($containerBuilder);

// Build PHP-DI Container
$container = $containerBuilder->build();

// Create the application
$app = AppFactory::createFromContainer($container);

// Middleware
$middleware = require $rootPath . '/app/middleware.php';
$middleware($app);

// Routing
$routes = require $rootPath . '/app/routes.php';
$routes($app);

// Routing cache only for non-debug version
$settings = $container->get('settings');
if($settings['debug'] === false) {
    $app->getRouteCollector()->setCacheFile($settings['route_cache']);
}

// Routing middleware
$app->addRoutingMiddleware();
$container->set(RouteParserInterface::class, $app->getRouteCollector()->getRouteParser());

// Error handling middleware
$errorMiddleware = $app->addErrorMiddleware($settings['debug'], !$settings['debug'], false);
$errorHandler = $errorMiddleware->getDefaultErrorHandler();
$errorHandler->registerErrorRenderer('text/html', VikingRetro\Renderer\HtmlErrorRenderer::class);

// Run the application
$app->run();

