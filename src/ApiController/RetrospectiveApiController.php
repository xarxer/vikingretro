<?php
declare(strict_types=1);

namespace VikingRetro\ApiController;

use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Log\LoggerInterface;
use VikingRetro\Factories\JsonResponseFactory;

class RetrospectiveApiController
{
    protected LoggerInterface $logger;
    protected JsonResponseFactory $responseFactory;
    protected EntityManager $entityManager;

    public function __construct(LoggerInterface $log, JsonResponseFactory $responseFactory, EntityManager $em) {
        $this->logger = $log;
        $this->responseFactory = $responseFactory;
        $this->entityManager = $em;
    }

    public function list(Request $request, Response $response, array $args = []) : Response {
        $this->logger->debug("Invoked RetrospectiveApiController:list");
        $response->getBody()->write("STUB");
        return $response;
    }

    public function create(Request $request, Response $response, array $args = []) : Response {
        $this->logger->debug("Invoked RetrospectiveApiController:create");
        $response->getBody()->write("STUB");
        return $response;
    }
}