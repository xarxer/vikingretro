<?php
declare(strict_types=1);

namespace VikingRetro\ApiController;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Log\LoggerInterface;
use VikingRetro\Factories\JsonResponseFactory;
use VikingRetro\System\LDAPSettings;

class AuthApiController
{
    private LoggerInterface $logger;
    private JsonResponseFactory $responseFactory;
    private LDAPSettings $ldapSettings;

    public function __construct(LoggerInterface $log, JsonResponseFactory $responseFactory, LDAPSettings $ldapSettings)
    {
        $this->logger = $log;
        $this->responseFactory = $responseFactory;
        $this->ldapSettings = $ldapSettings;
    }

    public function authenticate(Request $request, Response $response, array $args = []) : Response {
        $this->logger->debug("Invoked AuthApiController:authenticate");

        $postData = array(
            'user' => $request->getParsedBody()['username'],
            'password' => $request->getParsedBody()['password']
        );

        $handle = ldap_connect($this->ldapSettings->getHostname());

        if(!$handle) {
            $this->logger->warning("Failed to connect to LDAP server: " . $this->ldapSettings->getHostname());
            return $this->responseFactory->createResponse(500, "Failed to connect to LDAP");
        }

        ldap_set_option($handle, LDAP_OPT_PROTOCOL_VERSION, 3);

        // Bind to a user which is allowed to search the server
        $_bindRdn = sprintf("cn=%s,%s", $this->ldapSettings->getAdminUser(), $this->ldapSettings->getRDN());
        $_bindPass = $this->ldapSettings->getAdminPassword();
        $bindResult = ldap_bind($handle, $_bindRdn, $_bindPass);

        if(!$bindResult) {
            $this->logger->warning("Failed to bind user: " . $_bindRdn);
            return $this->responseFactory->createResponse(500, "Failed to connect to LDAP");
        }

        $_searchDn = sprintf("cn=users,%s", $this->ldapSettings->getRDN());
        $_searchFilter = sprintf("uid=%s", $postData['user']);
        $searchResult = ldap_search($handle, $_searchDn, $_searchFilter);

        if(!$searchResult) {
            $this->logger->warning("Failed to search for user: " . $_searchDn . " | filter: " . $_searchFilter);
            return $this->responseFactory->createResponse(500, "Failed to connect to LDAP");
        }

        if(!ldap_count_entries($handle, $searchResult)) {
            $this->logger->warning("No such user found: " . $postData['user']);
            return $this->responseFactory->createResponse(500, "No such user");
        }

        $info = ldap_get_entries($handle, $searchResult);
        $subject_cn = $info[0]['cn'][0]; // This is what's returned by LDAP, deal with it

        // Now that we know the CN, we can authenticate!
        $login_str = sprintf("cn=%s,cn=users,%s", $subject_cn, $this->ldapSettings->getRDN());
        $login_result = ldap_bind($handle, $login_str, $postData['password']);

        if(!$login_result) {
            $this->logger->warning("Login failed for user: " . $postData['user']);
            return $this->responseFactory->createResponse(500, "Failed to log in, please try again later");
        }

        // var_dump((int)$info[0]['uidnumber'][0]);

        return $this->responseFactory->createResponse(200, "success");
    }
}