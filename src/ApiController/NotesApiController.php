<?php
declare(strict_types=1);

namespace VikingRetro\ApiController;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Log\LoggerInterface;
use VikingRetro\Entity\Note;
use VikingRetro\Factories\JsonResponseFactory;

class NotesApiController
{
    protected LoggerInterface $logger;
    protected JsonResponseFactory $responseFactory;
    protected EntityManager $entityManager;

    public function __construct(LoggerInterface $log, JsonResponseFactory $responseFactory, EntityManager $em)
    {
        $this->logger = $log;
        $this->responseFactory = $responseFactory;
        $this->entityManager = $em;
    }

    public function list(Request $request, Response $response, array $args = []) : Response {
        $this->logger->debug("Invoked NotesApiController:list");

        $all_notes = $this->entityManager->getRepository(Note::class)->findAll();
        return $this->responseFactory->createResponse(200, "Success", $all_notes);
    }

    public function create(Request $request, Response $response, array $args = []) : Response {
        $this->logger->debug("Invoked NotesApiController:create");
        $note = Note::fromRequest($request);

        try {
            $this->entityManager->persist($note);
            $this->entityManager->flush();

            return $this->responseFactory->createResponse(201, "Success");
        } catch (ORMException $e) {
            return $this->responseFactory->createResponse(500, $e->getMessage());
        }
    }
}