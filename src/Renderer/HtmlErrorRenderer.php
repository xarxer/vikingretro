<?php
declare(strict_types=1);

namespace VikingRetro\Renderer;

use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Slim\Interfaces\ErrorRendererInterface;
use Slim\Views\Twig;
use Throwable;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class HtmlErrorRenderer implements ErrorRendererInterface
{

    private $view;

    public function __construct(ContainerInterface $container) {
        $settings = $container->get('settings');
        try {
            $this->view = new Twig(
                $settings['view']['template_path'],
                $settings['view']['twig']
            );
        } catch (LoaderError $e) {
        }
    }

    /**
     * @param Throwable $exception
     * @param bool $displayErrorDetails
     *
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function __invoke(Throwable $exception, bool $displayErrorDetails): string
    {
        if($exception->getCode() == 404) {
            return $this->view->fetch('error/404.twig');
        }

        $title = '500 - ' . get_class($exception);
        if(is_a($exception, '\Slim\Exception\HttpException')) {
            $title = $exception->getTitle();
        }

        return $this->view->fetch('error/default.twig', [
            'title' => $title,
            'debug' => $displayErrorDetails,
            'type' => get_class($exception),
            'code' => $exception->getCode(),
            'message' => $exception->getMessage(),
            'file' => $exception->getFile(),
            'line' =>  $exception->getLine(),
            'trace' => $exception->getTraceAsString()
        ]);
    }
}