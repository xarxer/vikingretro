<?php
declare(strict_types=1);

namespace VikingRetro\Controller;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class RetrospectiveController extends BaseController
{
    public function show(Request $request, Response $response, array $args = []) : Response {
        $this->logger->debug("Invoked RetrospectiveController:show");
        return $this->render($response, 'retrospective.twig');
    }
}