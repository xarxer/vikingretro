<?php
declare(strict_types=1);

namespace VikingRetro\Controller;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\ORMException;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class ExampleController extends BaseController
{
    public function show(Request $request, Response $response, array $args = []) : Response {
        $this->logger->debug("Invoked ExampleController:show");
        return $this->render($response, 'example.twig', []);
    }
}