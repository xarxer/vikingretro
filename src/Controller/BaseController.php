<?php
declare(strict_types=1);

namespace VikingRetro\Controller;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManager;
use Slim\Interfaces\RouteParserInterface;
use Slim\Views\Twig;

abstract class BaseController
{
    protected $view;
    protected $logger;
    protected $em;
    protected $router;
    protected $container;

    public function __construct(ContainerInterface $container) {
        $this->view = $container->get(Twig::class);
        $this->logger = $container->get(LoggerInterface::class);
        $this->em = $container->get(EntityManager::class);
        $this->router = $container->get(RouteParserInterface::class);
        $this->container = $container;
    }

    protected function render(Response $response, string $template, array $params = []) : Response {
        return $this->view->render($response, $template, $params);
    }
}