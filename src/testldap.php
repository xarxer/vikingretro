<?php

// Connect to LDAP
$handle = ldap_connect("ldap://localhost");

if(!$handle) {
    die("Failed to connect to LDAP" . PHP_EOL);
}

// Apparently we need to use protocol version 3
ldap_set_option($handle, LDAP_OPT_PROTOCOL_VERSION, 3);

// Anonymous bind allows us to query the server for information
$bind_result = ldap_bind($handle, "cn=admin,dc=vikingretro,dc=com", "iamsecret");

if(!$bind_result) {
    die("Failed to do ADMIN bind" . PHP_EOL);
}

// I want to sign in as the user with this ID
$my_uid = "posj";
$uid_str = sprintf("uid=%s", $my_uid);

// Search for this UID
$search_result = ldap_search($handle, "cn=users,dc=vikingretro,dc=com", $uid_str);

if(!$search_result) {
    die("Failed to perform LDAP search " . $uid_str . PHP_EOL);
}

if(!ldap_count_entries($handle, $search_result)) {
    die("No entries returned by search" . PHP_EOL);
}

$info = ldap_get_entries($handle, $search_result);
$subject_cn = $info[0]['cn'][0]; // This is what's returned by LDAP, deal with it

// Now that we know the CN, we can authenticate!
$login_str = sprintf("cn=%s,cn=users,dc=vikingretro,dc=com", $subject_cn);
$login_result = ldap_bind($handle, $login_str, "testpassword");

if(!$login_result) {
    die("Failed to login" . PHP_EOL);
} else {
    echo "Logged in" . PHP_EOL;
    var_dump((int)$info[0]['uidnumber'][0]);
}
