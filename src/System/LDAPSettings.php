<?php

namespace VikingRetro\System;

class LDAPSettings
{
    private string $hostname;
    private string $rdn;
    private string $adminUser;
    private string $adminPassword;

    public function __construct(string $hostname, string $rdn, string $adminUser, string $adminPassword)
    {
        $this->hostname = $hostname;
        $this->rdn = $rdn;
        $this->adminUser = $adminUser;
        $this->adminPassword = $adminPassword;
    }

    public function getHostname(): string {
        return $this->hostname;
    }

    public function getRDN(): string {
        return $this->rdn;
    }

    public function getAdminUser(): string {
        return $this->adminUser;
    }

    public function getAdminPassword(): string {
        return $this->adminPassword;
    }
}