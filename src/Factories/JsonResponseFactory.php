<?php

namespace VikingRetro\Factories;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Psr7\Response;
use stdClass;

class JsonResponseFactory
{
    private function createJsonData(int $code, string $message, $result): string {
        if(is_null($result)) {
            $result = new stdClass();
        }

        $data = array('status' => $code, 'message' => $message, 'result' => $result);
        return json_encode($data);
    }

    private function createJsonResponse(): ResponseInterface {
        $response = new Response();

        // PhpStorm 2019.3.1 complains about this, but it's OK
        return $response->withHeader('Content-Type', 'application/json');
    }

    public function createResponse(int $code, string $message, $result = null): ResponseInterface {
        $response = $this->createJsonResponse();
        $response->getBody()->write($this->createJsonData($code, $message, $result));

        return $response;
    }

    /**
     * @inheritDoc
     */
    /*public function createResponse(int $code = 200, string $reasonPhrase = ''): ResponseInterface
    {
        $response = new Response();

        $data = array('status' => $code, 'message' => $this->message, 'result' => $this->result);
        $payload = json_encode($data);
        $response->getBody()->write($payload);

        return $response->withStatus($code)->withHeader('Content-Type', 'application/json');
    }*/
}