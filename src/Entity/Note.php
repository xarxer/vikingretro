<?php
declare(strict_types=1);

namespace VikingRetro\Entity;

use Doctrine\ORM\Mapping as ORM;
use Psr\Http\Message\ServerRequestInterface as Request;
use JsonSerializable;

/**
 * User
 *
 * @ORM\Table(name="notes")
 * @ORM\Entity
 */
class Note implements JsonSerializable
{

    public static function fromRequest(Request $request): Note {
        $post_data = $request->getParsedBody();
        $type = (int)$post_data['type'];
        $content = $post_data['content'];

        $note = new Note();
        $note->setType($type);
        $note->setContent($content);

        return $note;
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="string", length=255, nullable=false)
     */
    private $content;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer", nullable=false)
     */
    private $type;

    /**
     * Get id.
     *
     * @return int
     */
    public function id()
    {
        return $this->id;
    }

    /**
     * Get content.
     *
     * @return string
     */
    public function content()
    {
        return $this->content;
    }

    public function setContent(string $content)
    {
        $this->content = $content;
    }

    /**
     * Get type.
     *
     * @return int
     */
    public function type()
    {
        return $this->type;
    }

    public function setType(int $type)
    {
        $this->type = $type;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return array(
            'id' => $this->id(),
            'content' => $this->content(),
            'type' => $this->type()
        );
    }
}
