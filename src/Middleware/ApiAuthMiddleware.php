<?php

namespace VikingRetro\Middleware;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use SlimSession\Helper;
use VikingRetro\Factories\JsonResponseFactory;

class ApiAuthMiddleware implements MiddlewareInterface
{
    private Helper $sessionHelper;
    private LoggerInterface $log;
    private JsonResponseFactory $jsonFactory;

    public function __construct(Helper $sessionHelper, LoggerInterface $log, JsonResponseFactory $jsonFactory) {
        $this->log = $log;
        $this->sessionHelper = $sessionHelper;
        $this->jsonFactory = $jsonFactory;
    }

    /**
     * @inheritDoc
     */
    public function process(Request $request, RequestHandler $handler): Response
    {
        //if($this->sessionHelper->exists('authenticated')) {
            return $handler->handle($request);
        //}
    }
}