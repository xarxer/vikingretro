<?php

namespace VikingRetro\Middleware;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use Slim\Views\Twig;
use SlimSession\Helper;
use VikingRetro\Factories\JsonResponseFactory;

class AuthMiddleware implements MiddlewareInterface
{
    private Helper $sessionHelper;
    private LoggerInterface $log;

    public function __construct(Helper $sessionHelper, LoggerInterface $log, Twig $twig) {
        $this->log = $log;
        $this->sessionHelper = $sessionHelper;

        $twig->getEnvironment()->addGlobal('testvar', 'world');
    }

    /**
     * @inheritDoc
     */
    public function process(Request $request, RequestHandler $handler): Response
    {
        return $handler->handle($request);
    }
}